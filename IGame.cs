﻿namespace Test
{
    public interface IGame
    {
        void Winning(int guesses);
        int StartNewGame();
    }
}