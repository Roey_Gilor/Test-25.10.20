﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Security.Policy;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Test
{
    public class WordsGame : IGame
    {
        public WordsGame()
        {

        }
        private static string word = "";
        private static string lines = "";
        public int Guesses { get; private set; }
        private static int wordLength = 0;
        private static int rightGuesses = 0;
        private static char[] rightSoFar;
        private static string[] wordsArray = new string[]
        {
            "peace", "love", "school", "door", "bannana", "hello", "word", "computer", "wire", "twin", "independence"
        };
        private static string PickWord()
        {
            Random choose = new Random();
            word = wordsArray[choose.Next(0, wordsArray.Length - 1)];
            wordLength = word.Length;
            rightSoFar = new char[wordLength];
            for (int i = 0; i < word.Length; i++)
            {
                lines += "-";
            }
            return lines;
        }
        public int GuessLetter(char letter)
        {            
            if (word.Contains(letter.ToString().ToLower()))
            {
                int check = 0;
                for (int i = 0; i < word.Length; i++)
                {
                    if (word[i] == letter)
                    {
                        check++;
                        rightSoFar[i] = letter;
                    }
                }
                rightGuesses += check;
            }
            Guesses++;
            PrintArray(rightSoFar);
            Console.WriteLine(lines);
            return rightGuesses;
        }
        private void PrintArray(char[] arr)
        {
            foreach (var item in arr)
            {
                Console.Write(item);
            }
            Console.WriteLine();
        }
        public void Winning(int guesses)
        {
            Console.WriteLine($"Coungratolations! you won the game in {guesses} guesses");
        }
        public int GetGuesses()
        {
            return Guesses;
        }
        public int StartNewGame()
        {
            word = "";
            wordLength = 0;
            Guesses = 0;
            wordLength = 0;
            rightGuesses = 0;
            Console.Clear();
            lines = PickWord();
            Console.WriteLine();
            return wordLength;
        }
        public void AskToPlay()
        {
            int length = this.StartNewGame();
            int rightGuesses = 0;
            Console.WriteLine("Welcome! lets start new game");
            while (length != rightGuesses)
            {
                Console.WriteLine("Guess a letter");
                char letter = char.Parse(Console.ReadLine());
                rightGuesses = this.GuessLetter(letter);
            }
            this.Winning(this.GetGuesses());
            Console.WriteLine("Want to play again? 1 yes 0 no");
            if (int.Parse(Console.ReadLine()) == 1)
                AskToPlay();
            else
                Console.WriteLine("Have a good day!");
        }
    }
}
